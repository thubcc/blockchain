pragma solidity ^0.4.0;

contract Name {

    mapping (address => string) public name;

    function set(string _value) public returns (bool success) {
        name[msg.sender] = _value;
        return true;
    }

    function get(address _owner) public view returns (string val) {
        return name[_owner];
    }
}