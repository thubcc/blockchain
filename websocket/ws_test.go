package ws

import (
	"encoding/json"
	"fmt"
	"net"
	"testing"
	"time"

	"github.com/gorilla/websocket"
)

type Res struct{
    Seq int64
    Longitude float64
    Latitude float64
    Name string
}

func connHandle(conn *websocket.Conn) {
    defer func() {
        conn.Close()
    }()
    stopCh := make(chan int)
    go send(conn, stopCh)
    for {
        conn.SetReadDeadline(time.Now().Add(time.Millisecond * time.Duration(5000)))
        _, msg, err := conn.ReadMessage()
        if err != nil {
            close(stopCh)
            // 判断是不是超时
            if netErr, ok := err.(net.Error); ok {
                if netErr.Timeout() {
                    fmt.Printf("ReadMessage timeout remote: %v\n", conn.RemoteAddr())
                    return
                }
            }
            // 其他错误，如果是 1001 和 1000 就不打印日志
            if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseNormalClosure) {
                fmt.Printf("ReadMessage other remote:%v error: %v \n", conn.RemoteAddr(), err)
            }
            return
        }
        fmt.Println("收到消息：", string(msg))
    }
}

func send1(conn *websocket.Conn, res *Res) {
    for i := 0; i < 1; i++ {
        data,_ := json.Marshal(res);
        err := conn.WriteMessage(1, []byte(data))
        if err != nil {
            fmt.Println("send msg faild ", err)
            return
		}
        time.Sleep(time.Millisecond * 10)
    }
}

func send(conn *websocket.Conn, stopCh chan int) {
    var res = Res{
        0,
        120.0,
        40.0,
        "test",
    }
    send1(conn,&res)
    for {
        select {
        case <-stopCh:
            fmt.Println("connect closed")
            return
        case <-time.After(time.Second * 1):
            res.Seq ++
            res.Latitude -= 0.01
            send1(conn,&res)
        }
    }
}

func TestListen(t *testing.T) {
    Url := "ws://127.0.0.1:9001/msg"
	dut,err := NewWsServer(Url)
	if err != nil {
		t.Errorf("setup service failure: %v",err)
	}
	dut.RegistHandle(connHandle)
    go dut.Start()
    client := &Client{Url:Url}
    <-time.After(time.Second*2)
    err = client.Connect()
    if err != nil {
		t.Errorf("connect server failure: %v",err)
    }
    err = client.WriteMsg("Hello")
    if err != nil {
		t.Errorf("send to server failure: %v",err)
    }
    var msg = Res{}
    err = client.ReadMsg(&msg)
    if err != nil {
		t.Errorf("Read message failure: %v",err)
    }
    if msg.Name!="test" {
        t.Errorf("message failure")
    }
}