package ws

import (
	"fmt"

	"github.com/gorilla/websocket"
)

type Client struct{
	Url string
	conn *websocket.Conn
}

func(self *Client) Connect() error {
	// u,err := url.Parse(self.Url)
	// if err != nil {
	// 	return fmt.Errorf("Parse Url failure: %v",err)
	// }
	var err error
	self.conn,_,err = websocket.DefaultDialer.Dial(self.Url, nil)
	if err != nil {
		return fmt.Errorf("connect to server failure %s :%v",self.Url,err)
	}
	return nil
}

// func(self *Client) Write(msg []byte) (int,error) {
// 	n,err := self.conn.Write(msg)
// 	if err != nil {
// 		return n,fmt.Errorf("Write failure: %v",err)
// 	}
// 	return n,err
// }

// func(self *Client) Read(msg []byte) (int,error) {
// 	n,err := self.conn.Read(msg)
// 	if err != nil {
// 		return n,fmt.Errorf("Read failure: %v",err)
// 	}
// 	return n,err
// }

func(self *Client) WriteMsg(msg interface{}) (err error) {
	// data,err := json.Marshal(msg);
	// if err != nil {
	// 	return fmt.Errorf("Marshal failure: %v",err)
	// }
	err = self.conn.WriteJSON(msg)
	if err != nil {
		return fmt.Errorf("Write msg failure: %v",err)
	}
	return nil
}

func(self *Client) ReadMsg(msg interface{}) error {
	// var buf = make([]byte,1024)
	// n,err := self.Read(buf)
	// if err !=nil {
	// 	return fmt.Errorf("Read failure: %v",err)
	// }
	// err = json.Unmarshal(buf[:n],msg)
	// if err != nil {
	// 	return fmt.Errorf("Unmarshal failure: %x : %v",buf[:n],err)
	// }
	return self.conn.ReadJSON(msg)
}