package iplocation
import (
	"math"
	"testing"
)

var (
	db = &IPDB{DBFile:"../.resources/GeoLite2-City.mmdb"}
)

func TestLocation(t *testing.T) {
	err := db.Init()
	if err != nil {
		t.Errorf("open db failure: %v",err)
	}
	defer db.Close()
	addr := "13.57.239.130:2743" //"119.3.217.230:30303"
	Longitude,Latitude,err := db.Location(addr)
	if err != nil {
		t.Errorf("read addr %s failure: %v",addr,err)
	}
	if math.Abs(Latitude-34.7)>0.1 {
		t.Errorf("Latitude miss %s:(%f,%f)",addr,Longitude,Latitude)
	}
	if math.Abs(Longitude-113.7)>0.1 {
		t.Errorf("Longitude miss %s:(%f,%f)",addr,Longitude,Latitude)
	}
}