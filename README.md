# blockchain

#### 介绍
此项目为《区块链原理与应用》课程中的一些代码样例或代码片段

#### 软件架构

这里只是一些Go语言的代码样例或代码片段

#### 安装教程

1.  go
需要go1.11以上版本
```
export GO111MODULE=on
```
2.  geth
3. clone

```
git clone http://gitee.com/thubcc/blockchain
cd blockchain

```


#### 使用说明

1.  getBlock
```
go run utils\getBlock.go -p ipc -d chain/geth.ipc
```
or
```
go run utils\getBlock.go -p rpc -h 127.0.0.1:8545
```
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
