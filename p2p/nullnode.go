package p2p

import (
	"crypto/ecdsa"
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/p2p"
	"github.com/ethereum/go-ethereum/p2p/enode"
	"github.com/ethereum/go-ethereum/p2p/nat"
)

func NewNullNode(peers []*enode.Node,asymKey *ecdsa.PrivateKey) (server *p2p.Server, err error) {
	if asymKey == nil {
		asymKey,err = crypto.GenerateKey()
		if err != nil {
			return nil,fmt.Errorf("gen node kry: %v",err)
		} else {
			fmt.Println("Gen private key for new node")
		}
	}
	
	var nullRun = func(p *p2p.Peer,rw p2p.MsgReadWriter) error {
		for {
			<-time.After(time.Second*1)
			fmt.Println("alive")
		}
		return nil
	}
	
	protocol := p2p.Protocol{
		Name:"nil",
		Version:1,
		Length:256,
		Run:nullRun,
	}

	maxPeers := 50
	server = &p2p.Server{
		Config: p2p.Config{
			PrivateKey:     asymKey,
			MaxPeers:       maxPeers,
			Name:           common.MakeName("p2p find", "6.0"),
			Protocols:      []p2p.Protocol{protocol},
			NAT:            nat.Any(),
			BootstrapNodes: peers,
			StaticNodes:    peers,
			TrustedNodes:   peers,
		},
	}

	return server,nil
}