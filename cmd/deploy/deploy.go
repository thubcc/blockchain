package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/big"

	"gitee.com/thubcc/blockchain/accounts"
	"gitee.com/thubcc/blockchain/ethclient"
	"github.com/ethereum/go-ethereum/common"
)

var (
	argUrl      = flag.String("url", "http://localhost:8545", "rpc raw path")
	argContract = flag.String("c", "contract/o5g", "rpc raw path")
	argDataDir  = flag.String("dir", ".ethereum/chain", "ethereum data dir")
	argKeyPass  = flag.String("p", "931", "message's encryption password")
	argPrefix   = flag.String("u", "931", "account prefix")
	argNonce    = flag.Int("n", 0, "nonce")
)

func main() {

	flag.Parse()
	keyConfig := accounts.NewConfig(*argDataDir,true,0)
	prk,err := keyConfig.FindKey(*argPrefix,*argKeyPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	config := ethclient.NewConfig(nil,931,*argUrl)
	
	ext,err := config.NewExt(prk)
	if err != nil {
		fmt.Println(err)
		return
	}

	abi, err := ioutil.ReadFile(*argContract+".abi")
	if err != nil {
		fmt.Println(err)
		return
	}
	contract, err := ext.NewContract(abi,common.Address{})
	if err != nil {
		fmt.Println(err)
		return
	}
	code,err := ethclient.BinFile(*argContract+".bin")
	result, err := contract.Deploy(uint64(*argNonce),code,big.NewInt(1000_000_000_000),"B2K",uint8(2),"B2K")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Deploy",result.Hex())
}
