package main

import (
	"flag"
	"fmt"

	"gitee.com/thubcc/blockchain/accounts"
)
var (
	argStart = flag.String("s", "", "Lucky key start with")
	argEnd = flag.String("e", "", "Lucky key end with")
	argDataDir     = flag.String("dir", ".ethereum/chain", "ethereum data dir")
	argPass      = flag.String("password", "931", "message's encryption password")
	argMax = flag.Int64("m", 1000000, "Max search")
)

func main(){
	flag.Parse()
	config := accounts.NewConfig(*argDataDir,true,*argMax)
	if addr,err := config.LuckyKey(*argStart,*argEnd,*argPass); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("gen addr",addr.Hex())
	}
}