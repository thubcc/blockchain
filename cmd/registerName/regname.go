package main

import (
	"flag"
	"fmt"

	"gitee.com/thubcc/blockchain/accounts"
	"gitee.com/thubcc/blockchain/ethclient"
	"github.com/ethereum/go-ethereum/common"
)

var (
	argUrl      = flag.String("url", "http://localhost:8545", "rpc raw path")
	argDataDir  = flag.String("dir", ".ethereum/chain", "ethereum data dir")
	argKeyPass  = flag.String("p", "931", "message's encryption password")
	argPrefix   = flag.String("u", "931", "account prefix")
	argName     = flag.String("name", "范闲", "Name")
	argAddr     = flag.String("addr", "0x69B47aD51691905D4ae1B3b61B27164C1776D08B", "contract address")
	argNonce    = flag.Int("n", 0, "nonce")
)

func main() {

	flag.Parse()
	keyConfig := accounts.NewConfig(*argDataDir,true,0)
	prk,err := keyConfig.FindKey(*argPrefix,*argKeyPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	config := ethclient.NewConfig(nil,931,*argUrl)
	
	ext,err := config.NewExt(prk)
	if err != nil {
		fmt.Println(err)
		return
	}

	if err != nil {
		fmt.Println(err)
		return
	}
	contract, err := ext.NewContract([]byte(abi),common.HexToAddress(*argAddr))
	if err != nil {
		fmt.Println(err)
		return
	}
	myname := *argName
	result, err := contract.CallEx(uint64(*argNonce),"set", myname)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Transfer",result.Hex())
}

var(
	abi = `[
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "name",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_value",
				"type": "string"
			}
		],
		"name": "set",
		"outputs": [
			{
				"name": "success",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_owner",
				"type": "address"
			}
		],
		"name": "get",
		"outputs": [
			{
				"name": "val",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]	
`
)