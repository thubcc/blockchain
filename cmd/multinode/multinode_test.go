package main

import (
	"testing"

	"github.com/ethereum/go-ethereum/p2p/enode"
)

func TestLog(t *testing.T) {
	if Log2(8)!=4 {
		t.Error("Log2 8 failure")
	}
}

func TestID(t *testing.T) {
	key := GenKey(1,3)
	id := enode.PubkeyToIDV4(&key.PublicKey)
	if id[0]&0xe0 != 0x20 {
		t.Error("Gen key:",id)
	}
}