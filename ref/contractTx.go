package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/big"

	"gitee.com/thubcc/blockchain/accounts"
	"gitee.com/thubcc/blockchain/ethclient"
	"github.com/ethereum/go-ethereum/common"
)

var (
	argUrl      = flag.String("url", "http://localhost:8545", "rpc raw path")
	argContract = flag.String("c", "contract/o5g", "rpc raw path")
	argDataDir  = flag.String("dir", ".ethereum/chain", "ethereum data dir")
	argKeyPass  = flag.String("p", "931", "message's encryption password")
	argPrefix   = flag.String("u", "931", "account prefix")
	argTo       = flag.String("to", "0xbfadc8b8497d4db8abf422abc7814a90e29f6e08", "to account")
	argNonce    = flag.Int("n", 0, "nonce")
)

func main() {

	flag.Parse()
	keyConfig := accounts.NewConfig(*argDataDir,true,0)
	prk,err := keyConfig.FindKey(*argPrefix,*argKeyPass)
	if err != nil {
		fmt.Println(err)
		return
	}
	config := ethclient.NewConfig(nil,931,*argUrl)
	
	ext,err := config.NewExt(prk)
	if err != nil {
		fmt.Println(err)
		return
	}

	abi, err := ioutil.ReadFile(*argContract+".abi")
	if err != nil {
		fmt.Println(err)
		return
	}
	contract, err := ext.NewContract(abi,common.HexToAddress("0x38671e119fd37e36a8f2a3fa3edd8478e9c69dc2"))
	if err != nil {
		fmt.Println(err)
		return
	}
	result, err := contract.CallEx(uint64(*argNonce),"transfer", common.HexToAddress(*argTo), big.NewInt(1000_000))
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Transfer",result.Hex())
}
