package main

import (
	_ "encoding/binary"
	"flag"
	"fmt"
	_ "net"
	_ "time"
	_ "context"
	_ "strconv"
	_ "strings"


	"github.com/ethereum/go-ethereum/p2p/enode"
	_ "github.com/ethereum/go-ethereum/p2p/netutil"
	_ "github.com/ethereum/go-ethereum/p2p/enr"

)

var (
	defaultNode = "enode://cb4d1b0d694111759058d3f7b77a1b1e6445cc06da8e1f61529631478c6b072f8889b94d59fd1f6d713fcddf798054cd13d38fb74a820f540aee9ab64bbbde0f@192.168.1.2:30303"
	defaultEnr  = "enr:-Je4QPbNBJhyUCxp8Yd_QX0KEA38VOv3AnjpBYvCOucAiP9fXf_kiFoqpcnH1YNwUM-9TM06UXMZRngZYdDs_hQS4oxug2V0aMfGhOmAUbGAgmlkgnY0gmlwhMCoAQKJc2VjcDI1NmsxoQPLTRsNaUERdZBY0_e3ehseZEXMBtqOH2FSljFHjGsHL4N0Y3CCdl-DdWRwgnZf"
	enodeString = flag.String("n", defaultNode, "enode")
)

const (
	// devp2p message codes
	handshakeMsg = 0x00
	discMsg      = 0x01
	pingMsg      = 0x02
	pongMsg      = 0x03
)

func main() {
	n := enode.MustParse(*enodeString)
	fmt.Println(n.String())
	// w MsgWriter, msgcode uint64
	SendItems(w, discMsg)
}

func (t *rlpx) doProtoHandshake(our *protoHandshake) (their *protoHandshake, err error) {
	// Writing our handshake happens concurrently, we prefer
	// returning the handshake read error. If the remote side
	// disconnects us early with a valid reason, we should return it
	// as the error so it can be tracked elsewhere.
	werr := make(chan error, 1)
	go func() { werr <- Send(t.rw, handshakeMsg, our) }()
	if their, err = readProtocolHandshake(t.rw); err != nil {
		<-werr // make sure the write terminates too
		return nil, err
	}
	if err := <-werr; err != nil {
		return nil, fmt.Errorf("write error: %v", err)
	}
	// If the protocol version supports Snappy encoding, upgrade immediately
	t.rw.snappy = their.Version >= snappyProtocolVersion

	return their, nil
}