package main

import (
	"flag"
	"fmt"
	"io/ioutil"

	"gitee.com/thubcc/blockchain/ethclient"
	"github.com/ethereum/go-ethereum/common"
)
// cmd arguments
var (
	argAddr   = flag.String("addr", "0x69B47aD51691905D4ae1B3b61B27164C1776D08B", "contract address")
	argAbi    = flag.String("abi", "contract/name.abi", "contract abi")
	argUrl    = flag.String("url", "http://localhost:8545", "ethereum rpc")
)

func main() {
	flag.Parse()

	config := ethclient.NewConfig(nil,931,*argUrl)
	
	ext,err := config.NewExt(nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	
	abiFile, err := ioutil.ReadFile(*argAbi)
	if err != nil {
		fmt.Println(err)
		return
	}

	addr := common.HexToAddress(*argAddr)
	
	contract, err := ext.NewContract(abiFile,addr)
	if err != nil {
		fmt.Println(err)
		return
	}
	v := ""
	_, err = contract.Call(&v, common.HexToAddress("0xbfadc8b8497d4db8abf422abc7814a90e29f6e08"),
		"get",common.HexToAddress("0xa4a880757e238dff54413b2ebe37e1c5463837fe"))
	if err != nil {
		fmt.Println("Call failure",err)
		return
	}

	fmt.Println(v)
}
