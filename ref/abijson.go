package main
import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/ethereum/go-ethereum/accounts/abi"
)
func main() {
	abiJson, _ := ioutil.ReadFile("contract/o5g.abi")
	reader := bytes.NewReader(abiJson)
	dec := json.NewDecoder(reader)
	var r abi.ABI
	err := dec.Decode(&r)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(r)
}